# Iris

A training wheel: Deployment of a multi-class classification model for the [Iris dataset](https://www.openml.org/search?type=data&status=active&id=61).

* [Building the model in a ML pipeline](notebooks)
* [Packaging the model using Tox](production-model-package)
* [Exposing using FastAPI](api)
* [CI and publishing](ci-and-publishing)
* [Deploying ML API with containers](deploying-with-containers)
* [Differential testing in CI](differential-testing)
* [Deploying to Iaas](iaas)